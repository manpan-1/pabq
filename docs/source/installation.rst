.. highlight:: shell

============
Installation
============


Stable release
--------------
The package does not exist in PyPI.

From sources
------------

The sources for Pabq can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/manpan-1/pabq

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com/manpan-1/pabq/tarball/master

Once you have a copy of the source, you need to add the repository directory to the path of Abaqus in order to import modules.
