=====
Usage
=====

To use Pabq, you need to have a working version of Abaqus and add the package's directory to Abaqus 'path'. Then, it is possible to import modules in the Python interpreter provided by Abaqus and use the individual tools or the modellers.

