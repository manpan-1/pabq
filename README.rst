====
Pabq
====


.. image:: https://img.shields.io/pypi/v/pabq.svg
        :target: https://pypi.python.org/pypi/pabq

.. image:: https://img.shields.io/travis/manpan-1/pabq.svg
        :target: https://travis-ci.com/manpan-1/pabq

.. image:: https://readthedocs.org/projects/pabq/badge/?version=latest
        :target: https://pabq.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Pythonic tools for modelling with Abaqus


* Free software: MIT license
* Documentation: https://pabq.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
