#!/bin/bash

################################################################
# Script to execute an abaqus job on slurm allocated resources #
################################################################

################################################################
##
## INSTRUCTIONS
##
## The first part of the current script is used to define the
## slurm options. These options are given as arguments to the
## sbatch command. Lines starting with one # symbol followed by
## SBATCH are active options. They can be deactivated by
## presceding an extra # symbol.
## The second part prepares the environment for abaqus to run.
## The job is executed in the current working dirwhere the output
## files of the execution are stored as a check file (.ckf).
## The third part executes abaqus. The execution is followed
## by arguments that set several options. The default options
## are working but they could be changed if needed.
##
## HOW TO USE
##
## Copy the current script (abaqus_run.sh) in the directory
## where the input file is located. From this directory, run
## sbatch followed by the name of this script followed by the
## abaqus job name, which should be the filename of the input
## file without the .inp extension.
##
## eg:
## sbatch abaqus_run.sh Job-1
##
## The example above submits the Job-1.inp and the all the
## output files located in the newly created subdirectory are
## named after that. The example command is executed without the
## presceding double # symbol.
################################################################



#############################
# Part 1: Slurm job options #
#############################

# The name of the project the request is made for.
#SBATCH -A SNIC2018-5-88 

# The name of the job.
#SBATCH -J abaqus

# Duration for which the resources are recuested.
#SBATCH -t 00:30:00

# Number of requested nodes
#SBATCH -N 1

# Number of cpus
##SBATCH -c

#  Requested memory size in MB. Not needed.
##SBATCH --mem=15000

## Requeste memory per cpu
##SBATCH --mem-per-cpu=5000

# Number of MPI processes per node
#SBATCH --ntasks-per-node=4

# Make nodes use exclusive. With this option, slurm allocates
# one entire node (48 cpus) regardless of how many are requested.
##SBATCH --exclusive

# Specify different files for output and error. If not given
# slurm returns one file with unique filename contaning both
# the output and error logs.
##SBATCH -e %.e
##SBATCH -o %.o


###################################
# Part 2: Prepare the environment #
###################################


# load necessary modules
module add intel/2017a

# The input file
inputFile=$1

ncpus=`echo $SLURM_NTASKS_PER_NODE`
ntcpus=`expr $SLURM_NNODES \* $ncpus`

envFile=abq_env_$SLURM_JOBID.env

echo "import os" > $envFile
echo "os.environ['ABA_BATCH_OVERRIDE'] = '1'" >> $envFile
#echo "verbose=3" >> $envFile

node_list=nodes_$SLURM_JOBID
srun hostname | sort  -u > ${node_list}

mp_host_list="["
for i in $(cat ${node_list})
do
    mp_host_list="${mp_host_list}['$i', $ncpus],"
done

mp_host_list=`echo ${mp_host_list} | sed -e "s/,$//"`
mp_host_list="${mp_host_list}]"

#export mp_host_list

echo "mp_host_list=${mp_host_list}"  >>$envFile 
echo "cpus=${ntcpus}" >> $envFile

# Unsetting the enviroment variable SLURM_GTIDS to fix the libhwloc.so problem
unset SLURM_GTIDS

# Echo several useful environment variables
echo "Printing some useful environment variables:"
echo "$SLURM_NTASKS: "$SLURM_NTASKS
echo "$SLURM_NPROC: "$SLURM_NPROC
echo "$SLURM_NNODES: "$SLURM_NNODES
echo "$SLURM_NTASKS_PER_NODE: "$SLURM_NTASKS_PER_NODE
echo "$SLURM_TASKS_PER_NODE: "$SLURM_TASKS_PER_NODE
echo "$SLURM_JOB_NODELIST: "$SLURM_JOB_NODELIST
echo "$SLURM_NODELIST: "$SLURM_NODELIST
echo "$SLURM_SUBMIT_HOST: "$SLURM_SUBMIT_HOST
echo "$SLURMD_NODENAME: "$SLURMD_NODENAME
echo "$HOSTNAME: "$HOSTNAME

############################
# Part 3: Abaqus execution #
############################


# Run verification that abaqus can accept user subrutines (checks intel compiler linkage)
#abq6141 verify -user_std

# Run Abaqus with the given python script
abq6141 cae noGUI=./$inputFile
#abq6141 job=$inputFile interactive user=GN_Riks_killer.f cpus=$SLURM_NTASKS
#move the output file in the job directory
#mv -f ../slurm-$SLURM_JOBID.out .
