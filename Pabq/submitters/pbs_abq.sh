#!/bin/sh
# This script is used to execute an abaqus session in a parallel processing 
# environment on a PBS system. The parameters for the execution are set as
# commented lines starting with '#PBS'.

###############################################################################
# Initially, a number of parameters have to be defined for the requested
# resources.

# Define a name for the job
#PBS -N abq_batch

# State the project number. Even though the pr.no. is given with capital 
# characters, it has to be given in lowercase here.
#PBS -A nn9674k

# Set the requested number of cpus, (look through)
#PBS -l select=1:ncpus=32:mpiprocs=16

# Define the time that the resources are going to be used.
#PBS -l walltime=20:00:00

###############################################################################
# Now that the resource request parameters are set, the environment for the
# execution of abaqus is being prepared in the provided system.

# Load the Abaqus module
module load abaqus/6.14-1

# Load the intel compilers
module load intelcomp

# First of all, define which modeler is going to be used
modeler=rhs_t_joint.py

# Give a project name for the current execution
prj_name=sp06

# Mirror the current working directory to the "work" tree.
mirdir=/work/panagima/$prj_name
rsync -r --ignore-existing --include=*/ --exclude=.* $PBS_O_WORKDIR/../../* $mirdir

# Move into the new work directory
cd $mirdir

# Print out some useful info
si=sys_info_$PBS_JOBID.log
echo "Current working directory" > $si
echo "=========================" >> $si
echo $PWD >> $si

echo "Hostnames" >> $si
echo "=========" >> $si
cat $PBS_NODEFILE >> $si
hostname >> $si

# Submit job command
# $abqexe job=$jobName cpus=$NSLOTS parallel=DOMAIN mp_mode=mpi double inter scratch=$Work
abaqus cae noGUI=Pabq/modelers/$modeler -- $prj_name parametric

