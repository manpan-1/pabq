## -*- coding: utf-8 -*-
#
#"""Sub-package of `Pabq` containing Abaqus modelling scripts."""
#
#__author__ = """Panagiotis Manoleas"""
#__email__ = 'panagiotis.manoleas@paramatrixab.com'
#__version__ = '0.1'
#__release__ = '0.1.0'
#__all__ = [
#    'bolt',
#    'closed_polygons',
#    'connector',
#    'dt_rhs',
#    'laced_angle',
#    'rhs_t_joint',
#]
#
#import Pabq.modelers.closed_polygons
