import logging


logging.basicConfig(filename="01.log", level=logging.INFO)

def sqrt(n):
    return n**0.5


def radius_from_thickness(thickness):
    # Corner radius calculated acc. to SSAB's rhs profile specs (taken from
    # document: "STRUCTURAL HOLLOW SECTIONS - dimensions and
    # cross-sectional properties-2016-Confetti"
    if thickness <= 6.:
        radius = 2 * thickness
    elif (thickness > 6.) and (thickness <=10.):
        radius = 2.5 * thickness
    else:
        radius = 3 * thickness
    return(radius)


def classification(width, thick, alfa, psi, epsilon):
    if plastic_allowed(width, thick, alfa, epsilon):
        if class1(width, thick, alfa, epsilon):
            pl_class = 1
        else:
            pl_class = 2
    else:
        if class4(width, thick, psi, epsilon):
            pl_class = 4
        else:
            pl_class = 3

    return(pl_class)


def plastic_allowed(cccc, thick, alfa, epsilon):
    ndwidth = cccc/(thick*epsilon)
    if alfa > -0.5:
        criterion = 456./(13*alfa - 1)
    else:
        criterion = 41.5/alfa

    if ndwidth <= criterion:
        status = True
        logging.info("Plastic stress distribution is allowed")
    else:
        status = False
        logging.info("Only elastic distribution is allowed")
    return status


def class1(cccc, thick, alfa, epsilon):
    ndwidth = cccc/(thick*epsilon)
    status = False
    if alfa > -0.5:
        criterion = 396./(13*alfa - 1)
    else:
        criterion = 36./alfa

    if ndwidth <= criterion:
        status = True

    logging.info(
        "epsilon, ndwidth, criterion: %.3f, %.3f, %.3f, " % (
            epsilon,
            ndwidth,
            criterion
        )
    )
    return status


def class4(cccc, thick, psi, epsilon):
    ndwidth = cccc/(thick*epsilon)
    status = False
    if psi > -1:
        criterion = 42 / (0.67 + 0.33*psi)
    else:
        criterion = 62*(1-psi)*(-psi)**0.5

    if ndwidth >= criterion:
        status = True

    logging.info(
        "epsilon, ndwidth, criterion: %.3f, %.3f, %.3f, " % (
            epsilon,
            ndwidth,
            criterion
        )
    )
    return status


def eff_witdh(bbbb, thick, psi, epsilon, internal=True):
    # Distinguish between internal and outstanding elements
    if internal:
        # Calculate k_sigma based on psi
        if (psi <= 1) and (psi >= 0):
            k_sigma = 8.2 / (1.05 + psi)
        elif (psi < 0) and (psi >= -1):
            k_sigma = 7.81 - 6.29*psi + 9.78*psi**2
        elif (psi < -1) and (psi > -3):
            k_sigma = 5.98*(1 - psi)**2
        else:
            logging.error("Wrong psi value.")
            return

        lamda_p = (bbbb/thick) / (28.4 * epsilon * sqrt(k_sigma))
        logging.info("lamda_p: %.3f, ", lamda_p)

        if lamda_p > 0.673:
            logging.info("Reduced to effective width.")
            rho = (lamda_p-0.055*(3+psi)) / lamda_p**2
            if rho > 1:
                rho = 1.
        else:
            logging.info("No effective width reduction")
            rho = 1.

        logging.info("rho: %.3f, " % rho)

    else:
        # Calculate k_sigma based on psi
        if (psi <= 1) and (psi >=0):
            k_sigma = 0.578 / (psi + 0.34)
        elif (psi < 0) and (psi >= -1):
            k_sigma = 1.7 - 5*psi + 17.1*psi**2
        else:
            logging.error("Wrong psi value.")
            pass

        lamda_p = (bbbb/thick) / (28.4 * epsilon * sqrt(k_sigma))
        logging.info("lamda_p: %.3f" % lamda_p)

        if lamda_p > 0.748:
            logging.info("Reduced to effectice width")
            rho = (lamda_p - 0.188)/lamda_p**2
            if rho > 1:
                rho = 1.
        else:
            logging.info("No effective width reduction")
            rho = 1.0

    # Compression zone width
    if psi < 0:
        b_c = bbbb / (1 - psi)
    else:
        b_c = bbbb

    # Effective width
    b_eff = rho * b_c

    return(b_eff)


def rhs_moi(
    bflat,
    b_eff,
    hflat,
    cog_y,
    he1,
    he2,
    thick
):
    pi = 3.1415

    # Outer radius of the profile corners
    r_out = radius_from_thickness(thick)
    r_in = r_out - thick

    # Tension and comression web parts
    htcog = hflat/2. + cog_y
    hccog = hflat/2. - cog_y

    # Calculate distances to tension and compression flange midlines
    h2tm = htcog + r_out - thick/2
    h2cm = hccog + r_out - thick/2
    logging.info("h2tm, h2cm: %.3f, %.3f" % (h2tm, h2cm))

    # Calculate partial moments of inertia
    i_t_flange = (bflat * thick**3)/12. + bflat*thick*h2tm**2
    logging.info("I - i_t_flange: %.3f" % i_t_flange)
    i_c_flange = (b_eff * thick**3)/12. + b_eff*thick*h2cm**2
    logging.info("I - i_c_flange: %.3f" % i_c_flange)
    i_t_web = (thick*htcog**3)/12. + thick*htcog*(htcog/2.)**2
    logging.info("I - i_t_web: %.3f" % i_t_web)
    i_ef2web = (thick*he2**3)/12. + thick*he2*(he2/2.)**2
    logging.info("I - i_ef2web: %.3f" % i_ef2web)
    i_ef1web = (thick*he1**3)/12. + thick*he1*(hccog - he1/2.)**2
    logging.info("I - i_ef1web: %.3f" % i_ef1web)
    i_semitorus = (pi/8)*(r_out**4-r_in**4)
    logging.info("I - i_semitorus: %.3f" % i_semitorus)
    i_t_c = i_semitorus + (pi/2)*(r_out**2 - r_in**2)*htcog**2
    logging.info("I - i_t_c: %.3f" % i_t_c)
    i_c_c = i_semitorus + (pi/2)*(r_out**2 - r_in**2)*hccog**2
    logging.info("I - i_c_c: %.3f" % i_c_c)

    # Total moment of inertia
    i_total = i_t_flange +\
            i_c_flange +\
            2*(i_t_web + i_ef1web + i_ef2web) +\
            i_t_c +\
            i_c_c

    return(i_total)


def calc_loads(m_prc, f_yield=None, side=None, thick=None):
    pi = 3.141592653589793238462643383279502
    # Extra info to be replaced by self
    bbbb = side
    hhhh = side
    r_out = radius_from_thickness(thick)
    r_in = r_out - thick
    logging.info("r_out, r_in: %.3f, %.3f" % (r_out, r_in))

    bflat = bbbb - 2*r_out
    hflat = hhhh - 2*r_out

    # Get the distance of the midlines to the profile centre.
    b2m = (bbbb - thick)/2
    h2m = (hhhh - thick)/2

    logging.info("Flat sides: %.3f %.3f" % (bflat, hflat))

    area = 2 * thick * (bflat + hflat + pi * (r_out - thick/2))
    logging.info("Area: %.3f" % area)

    if f_yield is None:
        f_yield = 355.

    # Calculate the material parameter epsilon
    epsilon = sqrt(235./f_yield)

    # The flange is alwayys in pure compression
    psi_flange = 1.
    alfa_flange = 1.

    # Initial of pure bending stress distribution for the web
    psi_web_new = -1
    alfa_web_new = 0.5
    hcflat = hflat/2.
    htflat = hflat/2.
    cog_y = 0

    # Set a convergence tolerance.
    tol = 1e-6
    approx = 1
    iteration = 0

    while approx > tol:
        iteration += 1
        logging.info("==============")
        logging.info("Iteration: %.3f" % iteration)
        logging.info("==============")

        # Classification of the flange
        logging.info("FLANGE CLASSIFICATION")
        flange_class = classification(
            bflat,
            thick,
            alfa_flange,
            psi_flange,
            epsilon
        )
        logging.info("Flange class: %.3f" % flange_class)

        b_eff = bflat
        if flange_class == 4:
            b_eff = eff_witdh(bflat, thick, psi_flange, epsilon, internal=True)

        b_ineff = bflat - b_eff
        logging.info("    b_eff, b_ineff: %.3f, %.3f" % (b_eff, b_ineff))

        # Classification of the web
        logging.info("WEB CLASSIFICATION")

        # Accept the values of alfa and psi from the last iteration
        alfa_web = alfa_web_new
        psi_web = psi_web_new

        logging.info("Psi web: %.3f" % psi_web)
        web_class = classification(
            hflat,
            thick,
            alfa_web,
            psi_web,
            epsilon
        )
        logging.info("Web class: %.3f" % web_class)

        h_eff = hflat
        if web_class == 4:
            h_eff = eff_witdh(hflat, thick, psi_web, epsilon, internal=True)

        if psi_web >= 0:
            h_ineff = hflat - h_eff
        else:
            h_ineff = hcflat - h_eff
        logging.info("    h_eff, h_ineff: %.3f, %.3f" % (h_eff, h_ineff))

        cs_class = max(flange_class, web_class)
        logging.info("Cross-section class: %.3f" % cs_class)

        # Proceed with elastic or plastic design accorindingly
        logging.info("RESISTANCE")
        if cs_class < 3:
            # Moment of inertia (only for information, not used in plastic
            # design)
            i_total = rhs_moi(
                bflat,
                b_eff,
                hflat,
                0.,
                0.0*hflat/2,
                1.0*hflat/2.,
                thick
            )
            logging.info("Moment of inertia: %.3f" % i_total)

            # Partial plastic section modulii
            w_f_p = bflat*thick*h2m
            w_w_p = (hflat/2.)*thick*(hflat/4.)
            area_c = (pi/2)*(r_out**2 - r_in**2)

            # Formula for the centre of gravity of a semi-torus taken from
            # Roark, pg. 809 (there is an typo on the given formula: the
            # r_i^2 in the numerator should be r_i^3. Here, in the script,
            # it is correct)
            cog_c = hflat/2. + (4.*(r_out**3 - r_in**3)/(3*pi*(r_out**2 - r_in**2)))
            w_c_p = area_c*cog_c

            # Total plastic section modulus
            w_pl = 2*(w_f_p + 2*w_w_p + w_c_p)
            logging.info("w_pl: %.3f" % w_pl)

            # Plastic moment resistance
            m_pl_rd = w_pl * f_yield
            logging.info("m_pl: %.3f" % m_pl_rd)

            # Applied moment
            m_ed = m_pl_rd*m_prc
            logging.info("m_ed: %.3f" % m_ed)

            # Calculate the max compression utilization based on the interaction
            # formula of EN1993-1-1, eq. (6.39)
            a_w = (area - 2*bbbb*thick)/area
            if a_w > 0.5:
                a_w = 0.5
            logging.info("a_w: %.3f" % a_w)
            n_prc = m_prc*(0.5*a_w - 1) + 1
            logging.info("n percent: %.3f" % n_prc)

            # Calculate the compression resitance.
            n_m_pl_rd = area*f_yield*n_prc

            # Compression will be applied to the cs up to failure
            n_ed = n_m_pl_rd
            logging.info("n_ed: %.3f" % n_ed)

            # Calculate the compression zone height, alfa
            alfa_web_new = (n_ed/(f_yield*2*thick*hflat) + 1)/2.
            if alfa_web_new > 1:
                alfa_web_new = 1
            logging.info("Compression zone, alfa: %.3f" % alfa_web_new)

            approx = abs(alfa_web_new - alfa_web)
            logging.info("approximation: %.3f" % approx)

        else:

            # Calculate the effective area of the cross section
            a_eff = area - thick*(2*h_ineff + b_ineff)
            logging.info("a_eff: %.3f" % a_eff)

            # New distance from CoG to web's ineffective area
            if psi_web > 0:
                he1 = h_eff * 2./(5-psi_web)
                he2 = h_eff - he1
            else:
                he1 = 0.4*h_eff
                he2 = 0.6*h_eff

            logging.info("he1, he2: %.3f, %.3f" % (he1, he2))

            # Calculate the neutral axis for the effective cross-section (if
            # any). The y-axis is positive pointing on the compression side of
            # the acting moment.
            h_inef_cog = htflat + he2 + h_ineff/2. - hflat/2
            cog_num = thick*(-2*h_ineff*h_inef_cog - b_ineff*h2m)
            cog_den = area - thick*(2*h_ineff + b_ineff)
            cog_y = cog_num / cog_den
            logging.info("h_inef_cog, cog_y: %.3f, %.3f" % (h_inef_cog, cog_y))

            # Distance of the new CoG to the extreme fiber
            h_xtr = hhhh/2. - cog_y
            logging.info("xtrm fiber: %.3f" % h_xtr)

            # Calculate moment of inertia
            i_total = rhs_moi(
                bflat,
                b_eff,
                hflat,
                cog_y,
                he1,
                he2,
                thick
            )

            logging.info("MOI: %.3f" % i_total)

            # Calculate the elastic section modulus.
            w_elast = i_total / h_xtr
            logging.info("W_elastic: %.3f" % w_elast)

            # Calculate the elastic moment resistance
            m_el_rd = f_yield * w_elast
            logging.info("M_el_Rd: %.3f" % m_el_rd)

            # Calculate the applied moment
            logging.info("m_prc: %.3f" % m_prc)
            m_ed = m_prc * m_el_rd
            logging.info("M_ed: %.3f" % m_ed)

            # Calculate the bearing capacity to axial compression as an
            # interaction with the applied moment.
            n_ed = (f_yield - m_ed/w_elast)*a_eff
            logging.info("N_ed: %.3f:" % n_ed)

            # Calculate the extreme compressive and tensile stresses for the
            # entire profile
            # Due to moment and axial separately
            sigma_n = n_ed/a_eff
            sigma_c_m = h_xtr*m_ed/i_total
            sigma_t_m = (hhhh-h_xtr)*m_ed/i_total
            logging.info(
                "Stress due to axial, compr. moment, tens. moment:\
                         %.3f, %.3f, %.3f" % (
                             sigma_n, sigma_c_m, sigma_t_m
                         )
            )


            # Combined
            sigma_c = sigma_n + sigma_c_m
            sigma_t = sigma_n - sigma_t_m

            # Calculate the extreme compressive and tensile stresses for the
            # web only
            sigma_c_m_web = (h_xtr - r_out)*m_ed/i_total
            sigma_t_m_web = (hhhh-h_xtr-r_out)*m_ed/i_total
            logging.info(
                "Web stress due to compr. moment, tens. moment:\
                         %.3f, %.3f" % (
                             sigma_c_m_web, sigma_t_m_web
                         )
            )

            sigma_c_web = n_ed/area + sigma_c_m_web
            sigma_t_web = n_ed/area - sigma_t_m_web

            # Calculate the distribution factor, psi, for the web
            psi_web_new = sigma_t_web/sigma_c_web
            logging.info("psi_web_new: %.3f" % psi_web_new)

            # Calculate the new tension and compression zone heights of
            # the web
            if psi_web_new < 0:
                hcflat = abs(hflat/(1 - psi_web_new))
            else:
                hcflat = hflat

            htflat = hflat - hcflat
            logging.info("hcflat, htflat: %3.f, %3.f" % (hcflat, htflat))

            # Calculate the conveargens relative to the previous step
            approx = abs(psi_web_new - psi_web)
            logging.info("approximation: %.3f" % approx)

    logging.info("Total number of iterations: %.3f" % iteration)
    return(n_ed, m_ed)

def main(side, thick):
    calc_loads(0.25, f_yield=355., side=side, thick=thick)

