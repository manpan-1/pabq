# The status of the script is incomplete and non functional, possible future project.
from material import *
from part import *
from section import *
from interaction import *
from step import *
from job import *
from mesh import *
import odbAccess
import Pabq.common_tools.abq_tools as at
import pickle
import numpy as np

def modeler(
        name=None,
        grip_lengths=None,
        double_nut=None,
        add=None,
        write_input=None,
        save_cae=None,
        submit=None
):
    """
    Create an Abaqus model of a uniaxial bolt test.

    Parameters
    ----------
    name : str, optional
        Name of the model. This string is also used as a base name upon which alternative suffixes are appended for
        several sub objects of the model e.g. the jog is named as `name`_job.
        By default, a generic name "BoltAndNut" is used
    grip_lengths : array-like of two floats, optional
        Tow floats are expected in an array (list or tuple) giving the grip length, l_g (first float), and the threaded
        grip length, l_t (second float). Default implies l_g = 28, l_t = 10
    double_nut : bool, optional
        Single or double nut assembly. Default implies single nut.
    add : bool, optional
        Whether or not to add the model in the existing session or create a new session (deletes all other models). The
        default value is True (the model will be added to the current session, existing models remain untouched)
    write_input : bool, optional
        Whether or not to write the input file for the created model. Default is `False`
    save_cae : bool, optional
        Whether or not to save the model in a .cae file in the working directory. Default is `False`
    submit : bool, optional
        Whether or not to submit for analysis. Default is `False`

    Return
    ------
        `model` object

    Notes
    -----
    The modelling script is a continuation of the work of Erik Grimsmo [1]. The initial script was used to model 5
    uniaxial bolt tests. The current script is a complete revision allowing for a parametric definition of the bolt
    assembly grip length, the threaded part of the grip length and the number of nuts (single or double nut assembly).
    This way, an arbitrary bolt assembly can be modeled on demand for specific applications.
    Currently, the bolt diameter and the grade are fixed (M16 8.8).


    [1] E. L. Grimsmo, A. Aalberg, M. Langseth, and A. H. Clausen, "Failure modes of bolt and nut assemblies under
        tensile loading" Journal of Constructional Steel Research, vol. 126, pp. 15-25, Nov. 2016.

    """
    if name is None:
        name = 'dt_rhs'

    if add is None:
        add = True

    if add:
        mdb.Model(name)
    else:
        # Start new model database.
        Mdb()

        # Change the pre-existing model name.
        mdb.models.changeKey(
            fromName='Model-1',
            toName=name
        )

    if grip_lengths is None:
        grip_lengths = (28., 10.)

    if double_nut is None:
        double_nut = False

    n_nuts = 1
    if double_nut:
        n_nuts = 2

    if write_input is None:
        write_input = False

    if save_cae is None:
        save_cae = False

    if submit is None:
        submit = False

    # Assign model object to a variable.
    mdl = mdb.models[name]

    # Names given to various objects throughout the model
    names = {
        "model": name,
        "job": name + "_job",
    }

    # General input
    thick = 6.3
    height = 120.
    width = 120.

    
    column_sktch = mdl.ConstrainedSketch(
        name='column_sktch',
        sheetSize=200.0
    )
    
    column_sktch.Line(
        point1=(0.0, height),
        point2=(0.0, 0.0)
    )
    column_sktch.Line(
        point1=(0.0, 0.0),
        point2=(width/2, 0.0)
    )
    column_sktch.Line(
        point1=(width/2, 0.0),
        point2=(width/2, thick)
    )
    column_sktch.Line(
        point1=(width/2, thick),
        point2=(thick, thick)
    )
    column_sktch.Line(
        point1=(thick, thick),
        point2=(thick, height-thick)
    )
    column_sktch.Line(
        point1=(thick, height-thick),
        point2=(width/2, height-thick)
    )
    column_sktch.Line(
        point1=(width/2, height-thick),
        point2=(width/2, height)
    )
    column_sktch.Line(
        point1=(width/2, height),
        point2=(0.0, height)
    )
    column_sktch.FilletByRadius(
        curve1=column_sktch.geometry[2], 
        curve2=column_sktch.geometry[3],
        nearPoint1=(0, 0),
        nearPoint2=(0, 0),
        radius=3*thick
    )
    column_sktch.FilletByRadius(
        curve1=column_sktch.geometry[6], 
        curve2=column_sktch.geometry[7],
        nearPoint1=(0, 0),
        nearPoint2=(0, 0),
        radius=2*thick
    )
    
    column_part = mdl.Part(
        dimensionality=THREE_D,
        name='Part-1', 
        type=DEFORMABLE_BODY
    )

    column_part.BaseSolidExtrude(depth=500.0, sketch=
        column_sktch)

    # del column_sktch
    #
    # mdl.rootAssembly.DatumCsysByDefault(CARTESIAN)
    # mdl.rootAssembly.Instance(dependent=ON, name='Part-1-1',
    #     part=column_part)
    # mdl.rootAssembly.Instance(dependent=ON, name='Part-1-2',
    #     part=column_part)
    # mdl.rootAssembly.instances['Part-1-2'].translate(vector=(
    #     55.0, 0.0, 0.0))
    # mdl.rootAssembly.FaceToFace(clearance=0.0, fixedPlane=
    #     mdl.rootAssembly.instances['Part-1-1'].faces[4], flip=OFF
    #     , movablePlane=
    #     mdl.rootAssembly.instances['Part-1-2'].faces[8])
    # mdl.rootAssembly.features['Face to Face-1'].setValues(flip=1)
    # mdl.rootAssembly.regenerate()
    # mdl.rootAssembly.regenerate()
    # mdl.rootAssembly.makeIndependent(instances=(
    #     mdl.rootAssembly.instances['Part-1-1'],
    #     mdl.rootAssembly.instances['Part-1-2']))
    # mdl.rootAssembly.deleteFeatures(('Part-1-2',
    #     'Face to Face-1'))
    # mdl.ConstrainedSketch(name='__profile__', sheetSize=200.0)
    # mdl.sketches['__profile__'].Line(point1=(8.75, 25.0), point2=
    #     (-7.5, 25.0))
    # mdl.sketches['__profile__'].HorizontalConstraint(
    #     addUndoState=False, entity=
    #     mdl.sketches['__profile__'].geometry[2])
    # mdl.sketches['__profile__'].Line(point1=(-7.5, 25.0), point2=
    #     (-7.5, -12.5))
    # mdl.sketches['__profile__'].VerticalConstraint(addUndoState=
    #     False, entity=mdl.sketches['__profile__'].geometry[3])
    # mdl.sketches['__profile__'].PerpendicularConstraint(
    #     addUndoState=False, entity1=
    #     mdl.sketches['__profile__'].geometry[2], entity2=
    #     mdl.sketches['__profile__'].geometry[3])
    # mdl.sketches['__profile__'].Line(point1=(-7.5, -12.5),
    #     point2=(8.75, -12.5))
    # mdl.sketches['__profile__'].HorizontalConstraint(
    #     addUndoState=False, entity=
    #     mdl.sketches['__profile__'].geometry[4])
    # mdl.sketches['__profile__'].PerpendicularConstraint(
    #     addUndoState=False, entity1=
    #     mdl.sketches['__profile__'].geometry[3], entity2=
    #     mdl.sketches['__profile__'].geometry[4])
    # mdl.sketches['__profile__'].Line(point1=(8.75, -12.5),
    #     point2=(8.75, -8.75))
    # mdl.sketches['__profile__'].VerticalConstraint(addUndoState=
    #     False, entity=mdl.sketches['__profile__'].geometry[5])
    # mdl.sketches['__profile__'].PerpendicularConstraint(
    #     addUndoState=False, entity1=
    #     mdl.sketches['__profile__'].geometry[4], entity2=
    #     mdl.sketches['__profile__'].geometry[5])
    # mdl.sketches['__profile__'].Line(point1=(8.75, -8.75),
    #     point2=(-2.5, -8.75))
    # mdl.sketches['__profile__'].HorizontalConstraint(
    #     addUndoState=False, entity=
    #     mdl.sketches['__profile__'].geometry[6])
    # mdl.sketches['__profile__'].PerpendicularConstraint(
    #     addUndoState=False, entity1=
    #     mdl.sketches['__profile__'].geometry[5], entity2=
    #     mdl.sketches['__profile__'].geometry[6])
    # mdl.sketches['__profile__'].Line(point1=(-2.5, -8.75),
    #     point2=(-2.5, 21.25))
    # mdl.sketches['__profile__'].VerticalConstraint(addUndoState=
    #     False, entity=mdl.sketches['__profile__'].geometry[7])
    # mdl.sketches['__profile__'].PerpendicularConstraint(
    #     addUndoState=False, entity1=
    #     mdl.sketches['__profile__'].geometry[6], entity2=
    #     mdl.sketches['__profile__'].geometry[7])
    # mdl.sketches['__profile__'].Line(point1=(-2.5, 21.25),
    #     point2=(8.75, 21.25))
    # mdl.sketches['__profile__'].HorizontalConstraint(
    #     addUndoState=False, entity=
    #     mdl.sketches['__profile__'].geometry[8])
    # mdl.sketches['__profile__'].PerpendicularConstraint(
    #     addUndoState=False, entity1=
    #     mdl.sketches['__profile__'].geometry[7], entity2=
    #     mdl.sketches['__profile__'].geometry[8])
    # mdl.sketches['__profile__'].Line(point1=(8.75, 21.25),
    #     point2=(8.75, 25.0))
    # mdl.sketches['__profile__'].VerticalConstraint(addUndoState=
    #     False, entity=mdl.sketches['__profile__'].geometry[9])
    # mdl.sketches['__profile__'].PerpendicularConstraint(
    #     addUndoState=False, entity1=
    #     mdl.sketches['__profile__'].geometry[8], entity2=
    #     mdl.sketches['__profile__'].geometry[9])
    # mdl.sketches['__profile__'].ObliqueDimension(textPoint=(
    #     1.60245513916016, 28.5903587341309), value=50.0, vertex1=
    #     mdl.sketches['__profile__'].vertices[0], vertex2=
    #     mdl.sketches['__profile__'].vertices[1])
    # mdl.sketches['__profile__'].ObliqueDimension(textPoint=(
    #     5.31339263916016, -15.8554210662842), value=50.0, vertex1=
    #     mdl.sketches['__profile__'].vertices[2], vertex2=
    #     mdl.sketches['__profile__'].vertices[3])
    # mdl.sketches['__profile__'].ObliqueDimension(textPoint=(
    #     -45.2060356140137, -1.77108764648438), value=200.0, vertex1=
    #     mdl.sketches['__profile__'].vertices[1], vertex2=
    #     mdl.sketches['__profile__'].vertices[2])
    # mdl.sketches['__profile__'].ObliqueDimension(textPoint=(
    #     13.0241165161133, 23.3310890197754), value=6.3, vertex1=
    #     mdl.sketches['__profile__'].vertices[7], vertex2=
    #     mdl.sketches['__profile__'].vertices[0])
    # mdl.sketches['__profile__'].ObliqueDimension(textPoint=(
    #     12.7551193237305, -24.1905841827393), value=6.3, vertex1=
    #     mdl.sketches['__profile__'].vertices[3], vertex2=
    #     mdl.sketches['__profile__'].vertices[4])
    # mdl.sketches['__profile__'].DistanceDimension(entity1=
    #     mdl.sketches['__profile__'].vertices[6], entity2=
    #     mdl.sketches['__profile__'].geometry[3], textPoint=(
    #     -36.4714889526367, 16.4269981384277), value=6.3)
    # mdl.sketches['__profile__'].FilletByRadius(curve1=
    #     mdl.sketches['__profile__'].geometry[3], curve2=
    #     mdl.sketches['__profile__'].geometry[2], nearPoint1=(
    #     -40.7754592895508, 22.434455871582), nearPoint2=(-38.9821395874023,
    #     25.0346946716309), radius=20.0)
    # mdl.sketches['__profile__'].FilletByRadius(curve1=
    #     mdl.sketches['__profile__'].geometry[3], curve2=
    #     mdl.sketches['__profile__'].geometry[4], nearPoint1=(
    #     -40.9243011474609, -166.382858276367), nearPoint2=(-35.3100128173828,
    #     -175.299438476562), radius=20.0)
    # mdl.sketches['__profile__'].FilletByRadius(curve1=
    #     mdl.sketches['__profile__'].geometry[7], curve2=
    #     mdl.sketches['__profile__'].geometry[6], nearPoint1=(
    #     -34.6451950073242, -160.89762878418), nearPoint2=(-27.787956237793,
    #     -169.024505615234), radius=13.7)
    # mdl.sketches['__profile__'].FilletByRadius(curve1=
    #     mdl.sketches['__profile__'].geometry[7], curve2=
    #     mdl.sketches['__profile__'].geometry[8], nearPoint1=(
    #     -34.6451530456543, 7.67292404174805), nearPoint2=(-27.787914276123,
    #     18.5933952331543), radius=13.7)
    # mdl.sketches['__profile__'].delete(objectList=(
    #     mdl.sketches['__profile__'].constraints[63], ))
    # mdl.sketches['__profile__'].delete(objectList=(
    #     mdl.sketches['__profile__'].constraints[64], ))
    # mdl.sketches['__profile__'].delete(objectList=(
    #     mdl.sketches['__profile__'].constraints[55], ))
    # mdl.Part(dimensionality=THREE_D, name='Part-2', type=
    #     DEFORMABLE_BODY)
    # mdl.parts['Part-2'].BaseSolidExtrude(depth=200.0, sketch=
    #     mdl.sketches['__profile__'])
    # del mdl.sketches['__profile__']
    # mdl.rootAssembly.Instance(dependent=OFF, name='Part-2-1',
    #     part=mdl.parts['Part-2'])
    # mdl.rootAssembly.instances['Part-2-1'].translate(vector=(
    #     61.25, 0.0, 0.0))
    # mdl.rootAssembly.FaceToFace(clearance=0.0, fixedPlane=
    #     mdl.rootAssembly.instances['Part-1-1'].faces[4], flip=ON,
    #     movablePlane=
    #     mdl.rootAssembly.instances['Part-2-1'].faces[12])
    # mdl.rootAssembly.ParallelFace(fixedPlane=
    #     mdl.rootAssembly.instances['Part-1-1'].faces[3], flip=OFF
    #     , movablePlane=
    #     mdl.rootAssembly.instances['Part-2-1'].faces[5])
    # del mdl.rootAssembly.features['Parallel Face-1']
    # mdl.rootAssembly.EdgeToEdge(fixedAxis=
    #     mdl.rootAssembly.instances['Part-1-1'].edges[11], flip=
    #     OFF, movableAxis=
    #     mdl.rootAssembly.instances['Part-2-1'].edges[34])
    # mdl.rootAssembly.DatumPlaneByPrincipalPlane(offset=150.0,
    #     principalPlane=XYPLANE)
    # mdl.rootAssembly.DatumPlaneByPrincipalPlane(offset=350.0,
    #     principalPlane=XYPLANE)
    # mdl.rootAssembly.PartitionCellByDatumPlane(cells=
    #     mdl.rootAssembly.instances['Part-1-1'].cells.getSequenceFromMask(
    #     ('[#1 ]', ), ), datumPlane=mdl.rootAssembly.datums[12])
    # mdl.rootAssembly.PartitionCellByDatumPlane(cells=
    #     mdl.rootAssembly.instances['Part-1-1'].cells.getSequenceFromMask(
    #     ('[#1 ]', ), ), datumPlane=mdl.rootAssembly.datums[13])
    # del mdl.rootAssembly.features['Edge to Edge-1']
    # mdl.rootAssembly.FaceToFace(clearance=0.0, fixedPlane=
    #     mdl.rootAssembly.datums[12], flip=ON, movablePlane=
    #     mdl.rootAssembly.instances['Part-2-1'].faces[10])
    # mdl.rootAssembly.deleteFeatures(('Partition cell-1',
    #     'Partition cell-2'))
    # mdl.rootAssembly.EdgeToEdge(fixedAxis=
    #     mdl.rootAssembly.instances['Part-1-1'].edges[11], flip=
    #     OFF, movableAxis=
    #     mdl.rootAssembly.instances['Part-2-1'].edges[34])
    # mdl.rootAssembly.PartitionCellByDatumPlane(cells=
    #     mdl.rootAssembly.instances['Part-1-1'].cells.getSequenceFromMask(
    #     ('[#1 ]', ), ), datumPlane=mdl.rootAssembly.datums[12])
    # mdl.rootAssembly.PartitionCellByDatumPlane(cells=
    #     mdl.rootAssembly.instances['Part-1-1'].cells.getSequenceFromMask(
    #     ('[#1 ]', ), ), datumPlane=mdl.rootAssembly.datums[13])
    # mdl.rootAssembly.features['Face to Face-1'].suppress()
    # del mdl.rootAssembly.features['Face to Face-1']
    # mdl.rootAssembly.features['Edge to Edge-1'].suppress()
    # mdl.rootAssembly.suppressFeatures(('Partition cell-1',
    #     'Partition cell-2'))
    # mdl.rootAssembly.EdgeToEdge(fixedAxis=
    #     mdl.rootAssembly.instances['Part-1-1'].edges[17], flip=ON
    #     , movableAxis=
    #     mdl.rootAssembly.instances['Part-2-1'].edges[25])
    # mdl.rootAssembly.InstanceFromBooleanCut(cuttingInstances=(
    #     mdl.rootAssembly.instances['Part-1-1'], ),
    #     instanceToBeCut=mdl.rootAssembly.instances['Part-2-1'],
    #     name='Part-3', originalInstances=SUPPRESS)
    # mdl.rootAssembly.features['Part-1-1'].resume()
    # mdl.rootAssembly.resumeFeatures(('Partition cell-1',
    #     'Partition cell-2'))
    # mdl.parts['Part-3'].RemoveFaces(deleteCells=False, faceList=
    #     mdl.parts['Part-3'].faces.getSequenceFromMask(mask=(
    #     '[#affa0000 ]', ), ))
    # mdl.parts['Part-3'].RemoveFaces(deleteCells=False, faceList=
    #     mdl.parts['Part-3'].faces.getSequenceFromMask(mask=(
    #     '[#f0000 ]', ), ))
    # mdl.rootAssembly.regenerate()
    # mdl.parts['Part-3'].PartitionCellByExtendFace(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#1 ]',
    #     ), ), extendFace=mdl.parts['Part-3'].faces[2])
    # mdl.ConstrainedSketch(gridSpacing=9.0, name='__profile__',
    #     sheetSize=360.22, transform=
    #     mdl.parts['Part-3'].MakeSketchTransform(
    #     sketchPlane=mdl.parts['Part-3'].faces[18],
    #     sketchPlaneSide=SIDE1,
    #     sketchUpEdge=mdl.parts['Part-3'].edges[27],
    #     sketchOrientation=TOP, origin=(15.0, -90.000001, 346.85)))
    # mdl.parts['Part-3'].projectReferencesOntoSketch(filter=
    #     COPLANAR_EDGES, sketch=mdl.sketches['__profile__'])
    # mdl.sketches['__profile__'].Line(point1=(-3.14999999909492,
    #     90.000001), point2=(-20.25, 90.0000009999999))
    # mdl.sketches['__profile__'].HorizontalConstraint(
    #     addUndoState=False, entity=
    #     mdl.sketches['__profile__'].geometry[10])
    # mdl.sketches['__profile__'].ParallelConstraint(addUndoState=
    #     False, entity1=mdl.sketches['__profile__'].geometry[3],
    #     entity2=mdl.sketches['__profile__'].geometry[10])
    # mdl.sketches['__profile__'].Line(point1=(-20.25,
    #     90.0000009999999), point2=(-3.14999999909503, 74.25))
    # mdl.sketches['__profile__'].CoincidentConstraint(
    #     addUndoState=False, entity1=
    #     mdl.sketches['__profile__'].vertices[9], entity2=
    #     mdl.sketches['__profile__'].geometry[6])
    # mdl.sketches['__profile__'].Line(point1=(-3.14999999909503,
    #     74.25), point2=(-3.14999999909492, 90.000001))
    # mdl.sketches['__profile__'].VerticalConstraint(addUndoState=
    #     False, entity=mdl.sketches['__profile__'].geometry[12])
    # mdl.sketches['__profile__'].EqualLengthConstraint(entity1=
    #     mdl.sketches['__profile__'].geometry[10], entity2=
    #     mdl.sketches['__profile__'].geometry[12])
    # mdl.sketches['__profile__'].ObliqueDimension(textPoint=(
    #     -8.1668762207031, 92.4525166021118), value=10.0, vertex1=
    #     mdl.sketches['__profile__'].vertices[3], vertex2=
    #     mdl.sketches['__profile__'].vertices[8])
    # mdl.parts['Part-3'].SolidExtrude(depth=30.0,
    #     flipExtrudeDirection=OFF, sketch=
    #     mdl.sketches['__profile__'], sketchOrientation=TOP,
    #     sketchPlane=mdl.parts['Part-3'].faces[18],
    #     sketchPlaneSide=SIDE1, sketchUpEdge=
    #     mdl.parts['Part-3'].edges[27])
    # del mdl.sketches['__profile__']
    # mdl.parts['Part-3'].features['Solid extrude-1'].setValues(
    #     flipExtrudeDirection=True)
    # mdl.parts['Part-3'].regenerate()
    # mdl.parts['Part-3'].regenerate()
    # mdl.ConstrainedSketch(gridSpacing=13.45, name='__profile__',
    #     sheetSize=538.14, transform=
    #     mdl.parts['Part-3'].MakeSketchTransform(
    #     sketchPlane=mdl.parts['Part-3'].faces[23],
    #     sketchPlaneSide=SIDE1,
    #     sketchUpEdge=mdl.parts['Part-3'].edges[30],
    #     sketchOrientation=TOP, origin=(15.0, -90.000001, 153.15)))
    # mdl.parts['Part-3'].projectReferencesOntoSketch(filter=
    #     COPLANAR_EDGES, sketch=mdl.sketches['__profile__'])
    # mdl.sketches['__profile__'].Line(point1=(3.15000000090504,
    #     90.000001), point2=(11.4397338866256, 90.000001))
    # mdl.sketches['__profile__'].HorizontalConstraint(
    #     addUndoState=False, entity=
    #     mdl.sketches['__profile__'].geometry[12])
    # mdl.sketches['__profile__'].ParallelConstraint(addUndoState=
    #     False, entity1=mdl.sketches['__profile__'].geometry[9],
    #     entity2=mdl.sketches['__profile__'].geometry[12])
    # mdl.sketches['__profile__'].Line(point1=(11.4397338866256,
    #     90.000001), point2=(3.15000000090527, 80.7))
    # mdl.sketches['__profile__'].CoincidentConstraint(
    #     addUndoState=False, entity1=
    #     mdl.sketches['__profile__'].vertices[11], entity2=
    #     mdl.sketches['__profile__'].geometry[11])
    # mdl.sketches['__profile__'].Line(point1=(3.15000000090527,
    #     80.7), point2=(3.15000000090504, 90.000001))
    # mdl.sketches['__profile__'].VerticalConstraint(addUndoState=
    #     False, entity=mdl.sketches['__profile__'].geometry[14])
    # mdl.sketches['__profile__'].EqualLengthConstraint(entity1=
    #     mdl.sketches['__profile__'].geometry[12], entity2=
    #     mdl.sketches['__profile__'].geometry[14])
    # mdl.sketches['__profile__'].ObliqueDimension(textPoint=(
    #     6.13825073242188, 94.0570316777954), value=10.0, vertex1=
    #     mdl.sketches['__profile__'].vertices[8], vertex2=
    #     mdl.sketches['__profile__'].vertices[10])
    # mdl.parts['Part-3'].SolidExtrude(depth=30.0,
    #     flipExtrudeDirection=ON, sketch=
    #     mdl.sketches['__profile__'], sketchOrientation=TOP,
    #     sketchPlane=mdl.parts['Part-3'].faces[23],
    #     sketchPlaneSide=SIDE1, sketchUpEdge=
    #     mdl.parts['Part-3'].edges[30])
    # del mdl.sketches['__profile__']
    # mdl.parts['Part-3'].PartitionCellByExtendFace(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#2 ]',
    #     ), ), extendFace=mdl.parts['Part-3'].faces[17])
    # mdl.parts['Part-3'].PartitionCellByExtendFace(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#1 ]',
    #     ), ), extendFace=mdl.parts['Part-3'].faces[26])
    # mdl.parts['Part-3'].PartitionCellByPlanePointNormal(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#2 ]',
    #     ), ), normal=mdl.parts['Part-3'].edges[64], point=
    #     mdl.parts['Part-3'].vertices[25])
    # mdl.parts['Part-3'].PartitionCellByPlanePointNormal(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#2 ]',
    #     ), ), normal=mdl.parts['Part-3'].edges[12], point=
    #     mdl.parts['Part-3'].vertices[10])
    # mdl.parts['Part-3'].PartitionCellByPlanePointNormal(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#1 ]',
    #     ), ), normal=mdl.parts['Part-3'].edges[4], point=
    #     mdl.parts['Part-3'].vertices[4])
    # mdl.parts['Part-3'].PartitionCellByPlanePointNormal(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#4 ]',
    #     ), ), normal=mdl.parts['Part-3'].edges[33], point=
    #     mdl.parts['Part-3'].vertices[19])
    # mdl.parts['Part-3'].PartitionCellByPlanePointNormal(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#40 ]',
    #     ), ), normal=mdl.parts['Part-3'].edges[75], point=
    #     mdl.parts['Part-3'].vertices[30])
    # mdl.parts['Part-3'].setMeshControls(elemShape=HEX_DOMINATED,
    #     regions=mdl.parts['Part-3'].cells.getSequenceFromMask((
    #     '[#340 ]', ), ))
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=9.0)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=5.0)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=3.0)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=2.0)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=2.3)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=2.4)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=2.5)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=2.6)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=2.55)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=2.53)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=2.51)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=2.52)
    # mdl.parts['Part-3'].seedPart(deviationFactor=0.1,
    #     minSizeFactor=0.1, size=2.51)
    # mdl.parts['Part-3'].generateMesh()
    # mdl.parts['Part-3'].setMeshControls(elemShape=WEDGE, regions=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#100 ]',
    #     ), ), technique=SYSTEM_ASSIGN)
    # mdl.parts['Part-3'].generateMesh()
    # mdl.parts['Part-3'].setMeshControls(elemShape=TET, regions=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#100 ]',
    #     ), ), technique=FREE)
    # mdl.parts['Part-3'].setElementType(elemTypes=(ElemType(
    #     elemCode=C3D20R, elemLibrary=STANDARD), ElemType(elemCode=C3D15,
    #     elemLibrary=STANDARD), ElemType(elemCode=C3D10, elemLibrary=STANDARD)),
    #     regions=(mdl.parts['Part-3'].cells.getSequenceFromMask((
    #     '[#100 ]', ), ), ))
    # mdl.parts['Part-3'].generateMesh(regions=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#100 ]',
    #     ), ))
    # mdl.parts['Part-3'].DatumPlaneByTwoPoint(point1=
    #     mdl.parts['Part-3'].vertices[31], point2=
    #     mdl.parts['Part-3'].vertices[28])
    # mdl.parts['Part-3'].DatumPlaneByTwoPoint(point1=
    #     mdl.parts['Part-3'].vertices[23], point2=
    #     mdl.parts['Part-3'].vertices[6])
    # mdl.parts['Part-3'].deleteMesh(regions=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#1ae ]',
    #     ), ))
    # mdl.parts['Part-3'].PartitionCellByDatumPlane(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#100 ]',
    #     ), ), datumPlane=mdl.parts['Part-3'].datums[32])
    # mdl.parts['Part-3'].PartitionCellByDatumPlane(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#200 ]',
    #     ), ), datumPlane=mdl.parts['Part-3'].datums[33])
    # mdl.parts['Part-3'].RemoveFaces(deleteCells=False, faceList=
    #     mdl.parts['Part-3'].faces.getSequenceFromMask(mask=(
    #     '[#165 #408200 ]', ), ))
    # mdl.parts['Part-3'].features['Remove faces-3'].suppress()
    # del mdl.parts['Part-3'].features['Remove faces-3']
    # mdl.parts['Part-3'].RemoveFaces(deleteCells=False, faceList=
    #     mdl.parts['Part-3'].faces.getSequenceFromMask(mask=(
    #     '[#144 #408200 ]', ), ))
    # mdl.parts['Part-3'].generateMesh()
    # mdl.rootAssembly.regenerate()
    # mdl.rootAssembly.Surface(name='m_Surf-1', side1Faces=
    #     mdl.rootAssembly.instances['Part-1-1'].faces.getSequenceFromMask(
    #     ('[#800000 ]', ), ))
    # mdl.rootAssembly.Surface(name='s_Surf-1', side1Faces=
    #     mdl.rootAssembly.instances['Part-3-1'].faces.getSequenceFromMask(
    #     ('[#2 ]', ), ))
    # mdl.Tie(adjust=ON, master=
    #     mdl.rootAssembly.surfaces['m_Surf-1'], name=
    #     'Constraint-1', positionToleranceMethod=COMPUTED, slave=
    #     mdl.rootAssembly.surfaces['s_Surf-1'], thickness=ON,
    #     tieRotations=ON)
    # mdl.rootAssembly.Surface(name='m_Surf-3', side1Faces=
    #     mdl.rootAssembly.instances['Part-1-1'].faces.getSequenceFromMask(
    #     ('[#400000 ]', ), ))
    # mdl.rootAssembly.Surface(name='s_Surf-3', side1Faces=
    #     mdl.rootAssembly.instances['Part-3-1'].faces.getSequenceFromMask(
    #     ('[#80000000 ]', ), ))
    # mdl.Tie(adjust=ON, master=
    #     mdl.rootAssembly.surfaces['m_Surf-3'], name=
    #     'Constraint-2', positionToleranceMethod=COMPUTED, slave=
    #     mdl.rootAssembly.surfaces['s_Surf-3'], thickness=ON,
    #     tieRotations=ON)
    # mdl.rootAssembly.Surface(name='m_Surf-5', side1Faces=
    #     mdl.rootAssembly.instances['Part-1-1'].faces.getSequenceFromMask(
    #     ('[#800 ]', ), ))
    # mdl.rootAssembly.Surface(name='s_Surf-5', side1Faces=
    #     mdl.rootAssembly.instances['Part-3-1'].faces.getSequenceFromMask(
    #     ('[#20000000 ]', ), ))
    # mdl.Tie(adjust=ON, master=
    #     mdl.rootAssembly.surfaces['m_Surf-5'], name=
    #     'Constraint-3', positionToleranceMethod=COMPUTED, slave=
    #     mdl.rootAssembly.surfaces['s_Surf-5'], thickness=ON,
    #     tieRotations=ON)
    # mdl.rootAssembly.DatumPointByMidPoint(point1=
    #     mdl.rootAssembly.instances['Part-3-1'].vertices[37],
    #     point2=
    #     mdl.rootAssembly.instances['Part-3-1'].vertices[26])
    # mdl.rootAssembly.ReferencePoint(point=
    #     mdl.rootAssembly.datums[29])
    # mdl.rootAssembly.DatumPointByOffset(point=
    #     mdl.rootAssembly.instances['Part-1-1'].vertices[25],
    #     vector=(50.0, 0.0, 0.0))
    # mdl.rootAssembly.ReferencePoint(point=
    #     mdl.rootAssembly.datums[31])
    # mdl.rootAssembly.DatumPointByOffset(point=
    #     mdl.rootAssembly.instances['Part-1-1'].vertices[12],
    #     vector=(50.0, 0.0, 0.0))
    # mdl.rootAssembly.ReferencePoint(point=
    #     mdl.rootAssembly.datums[33])
    # mdl.rootAssembly.Set(name='m_Set-1', referencePoints=(
    #     mdl.rootAssembly.referencePoints[30], ))
    # mdl.rootAssembly.Surface(name='s_Surf-7', side1Faces=
    #     mdl.rootAssembly.instances['Part-3-1'].faces.getSequenceFromMask(
    #     ('[#6240000 #1000000 ]', ), ))
    # mdl.Coupling(controlPoint=
    #     mdl.rootAssembly.sets['m_Set-1'], couplingType=KINEMATIC,
    #     influenceRadius=WHOLE_SURFACE, localCsys=None, name='Constraint-4',
    #     surface=mdl.rootAssembly.surfaces['s_Surf-7'], u1=ON, u2=
    #     ON, u3=ON, ur1=ON, ur2=ON, ur3=ON)
    # mdl.rootAssembly.Set(name='m_Set-2', referencePoints=(
    #     mdl.rootAssembly.referencePoints[32], ))
    # mdl.rootAssembly.Surface(name='s_Surf-8', side1Faces=
    #     mdl.rootAssembly.instances['Part-1-1'].faces.getSequenceFromMask(
    #     ('[#8000000 ]', ), ))
    # mdl.Coupling(controlPoint=
    #     mdl.rootAssembly.sets['m_Set-2'], couplingType=KINEMATIC,
    #     influenceRadius=WHOLE_SURFACE, localCsys=None, name='Constraint-5',
    #     surface=mdl.rootAssembly.surfaces['s_Surf-8'], u1=ON, u2=
    #     ON, u3=ON, ur1=ON, ur2=ON, ur3=ON)
    # mdl.rootAssembly.Set(name='m_Set-3', referencePoints=(
    #     mdl.rootAssembly.referencePoints[34], ))
    # mdl.rootAssembly.Surface(name='s_Surf-9', side1Faces=
    #     mdl.rootAssembly.instances['Part-1-1'].faces.getSequenceFromMask(
    #     ('[#4000000 ]', ), ))
    # mdl.Coupling(controlPoint=
    #     mdl.rootAssembly.sets['m_Set-3'], couplingType=KINEMATIC,
    #     influenceRadius=WHOLE_SURFACE, localCsys=None, name='Constraint-6',
    #     surface=mdl.rootAssembly.surfaces['s_Surf-9'], u1=ON, u2=
    #     ON, u3=ON, ur1=ON, ur2=ON, ur3=ON)
    # mdl.rootAssembly.Set(faces=
    #     mdl.rootAssembly.instances['Part-1-1'].faces.getSequenceFromMask(
    #     ('[#2008008 ]', ), ), name='Set-4')
    # mdl.YsymmBC(createStepName='Initial', localCsys=None, name=
    #     'BC-1', region=mdl.rootAssembly.sets['Set-4'])
    # mdl.rootAssembly.Set(faces=
    #     mdl.rootAssembly.instances['Part-1-1'].faces.getSequenceFromMask(
    #     mask=('[#201040 ]', ), )+\
    #     mdl.rootAssembly.instances['Part-3-1'].faces.getSequenceFromMask(
    #     mask=('[#10010400 #900001 ]', ), ), name='Set-5')
    # mdl.XsymmBC(createStepName='Initial', localCsys=None, name=
    #     'BC-2', region=mdl.rootAssembly.sets['Set-5'])
    # mdl.rootAssembly.Set(name='Set-6', referencePoints=(
    #     mdl.rootAssembly.referencePoints[32],
    #     mdl.rootAssembly.referencePoints[34]))
    # mdl.DisplacementBC(amplitude=UNSET, createStepName='Initial',
    #     distributionType=UNIFORM, fieldName='', localCsys=None, name='BC-3',
    #     region=mdl.rootAssembly.sets['Set-6'], u1=SET, u2=SET,
    #     u3=SET, ur1=UNSET, ur2=UNSET, ur3=UNSET)
    # mdl.rootAssembly.Set(name='Set-7', referencePoints=(
    #     mdl.rootAssembly.referencePoints[30], ))
    # mdl.DisplacementBC(amplitude=UNSET, createStepName='Initial',
    #     distributionType=UNIFORM, fieldName='', localCsys=None, name='BC-4',
    #     region=mdl.rootAssembly.sets['Set-7'], u1=SET, u2=UNSET,
    #     u3=UNSET, ur1=UNSET, ur2=UNSET, ur3=SET)
    # mdl.ExplicitDynamicsStep(massScaling=((SEMI_AUTOMATIC, MODEL,
    #     AT_BEGINNING, 1000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ), name=
    #     'disp', previous='Initial', timePeriod=100.0)
    # mdl.Material(name='Material-1')
    # mdl.materials['Material-1'].Density(table=((7.9e-09, ), ))
    # mdl.materials['Material-1'].Elastic(table=((210000.0, 0.3),
    #     ))
    # mdl.HomogeneousSolidSection(material='Material-1', name=
    #     'Section-1', thickness=None)
    # column_part.Set(cells=
    #     column_part.cells.getSequenceFromMask(('[#1 ]',
    #     ), ), name='Set-1')
    # column_part.SectionAssignment(offset=0.0,
    #     offsetField='', offsetType=MIDDLE_SURFACE, region=
    #     column_part.sets['Set-1'], sectionName=
    #     'Section-1', thicknessAssignment=FROM_SECTION)
    # mdl.parts['Part-3'].Set(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#3ff ]',
    #     ), ), name='Set-6')
    # mdl.parts['Part-3'].SectionAssignment(offset=0.0,
    #     offsetField='', offsetType=MIDDLE_SURFACE, region=
    #     mdl.parts['Part-3'].sets['Set-6'], sectionName=
    #     'Section-1', thicknessAssignment=FROM_SECTION)
    # mdl.rootAssembly.regenerate()
    # mdl.SmoothStepAmplitude(data=((0.0, 0.0), (100.0, 1.0)),
    #     name='Amp-1', timeSpan=STEP)
    # mdl.boundaryConditions['BC-4'].setValues(ur1=SET, ur2=SET)
    # mdl.boundaryConditions['BC-4'].setValuesInStep(amplitude=
    #     'Amp-1', stepName='disp', ur1=1.0)
    # mdl.rootAssembly.generateMesh(regions=(
    #     mdl.rootAssembly.instances['Part-1-1'], ))
    # mdl.rootAssembly.setElementType(elemTypes=(ElemType(
    #     elemCode=C3D8R, elemLibrary=EXPLICIT, secondOrderAccuracy=OFF,
    #     kinematicSplit=AVERAGE_STRAIN, hourglassControl=DEFAULT,
    #     distortionControl=DEFAULT), ElemType(elemCode=C3D6, elemLibrary=EXPLICIT),
    #     ElemType(elemCode=C3D4, elemLibrary=EXPLICIT)), regions=(
    #     mdl.rootAssembly.instances['Part-1-1'].cells.getSequenceFromMask(
    #     ('[#7 ]', ), ), ))
    # mdb.Job(activateLoadBalancing=False, atTime=None, contactPrint=OFF,
    #     description='', echoPrint=OFF, explicitPrecision=SINGLE, historyPrint=OFF,
    #     memory=90, memoryUnits=PERCENTAGE, model='Model-1', modelPrint=OFF,
    #     multiprocessingMode=DEFAULT, name='wrhs_job', nodalOutputPrecision=SINGLE,
    #     numCpus=1, numDomains=1, parallelizationMethodExplicit=DOMAIN, queue=None,
    #     resultsFormat=ODB, scratch='', type=ANALYSIS, userSubroutine='', waitHours=
    #     0, waitMinutes=0)
    # mdl.parts['Part-3'].setElementType(elemTypes=(ElemType(
    #     elemCode=C3D8R, elemLibrary=EXPLICIT, secondOrderAccuracy=OFF,
    #     kinematicSplit=AVERAGE_STRAIN, hourglassControl=DEFAULT,
    #     distortionControl=DEFAULT), ElemType(elemCode=C3D6, elemLibrary=EXPLICIT),
    #     ElemType(elemCode=C3D4, elemLibrary=EXPLICIT)), regions=(
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#2fe ]',
    #     ), ), ))
    # mdl.parts['Part-3'].setElementType(elemTypes=(ElemType(
    #     elemCode=C3D8R, elemLibrary=EXPLICIT), ElemType(elemCode=C3D6,
    #     elemLibrary=EXPLICIT), ElemType(elemCode=C3D4, elemLibrary=EXPLICIT,
    #     secondOrderAccuracy=OFF, distortionControl=DEFAULT)), regions=(
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#1 ]',
    #     ), ), ))
    # mdl.rootAssembly.regenerate()
    # mdb.jobs['wrhs_job'].setValues(activateLoadBalancing=False, numCpus=4,
    #     numDomains=4)
    # mdb.jobs['wrhs_job'].setValues(activateLoadBalancing=False, explicitPrecision=
    #     DOUBLE_PLUS_PACK, nodalOutputPrecision=FULL, numDomains=4)
    # mdb.jobs['wrhs_job'].setValues(activateLoadBalancing=False,
    #     multiprocessingMode=THREADS, numDomains=4, parallelizationMethodExplicit=
    #     DOMAIN)
    # mdl.fieldOutputRequests['F-Output-1'].setValues(timeInterval=
    #     EVERY_TIME_INCREMENT)
    # mdl.steps['disp'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 100000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ),
    #     timeIncrementationMethod=FIXED_USER_DEFINED_INC, userDefinedInc=0.1)
    # mdl.steps['disp'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 100000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ),
    #     maxIncrement=None, scaleFactor=1.0, timeIncrementationMethod=
    #     AUTOMATIC_GLOBAL)
    # mdl.rootAssembly.Surface(name='Surf-10', side1Faces=
    #     mdl.rootAssembly.instances['Part-1-1'].faces.getSequenceFromMask(
    #     ('[#100 ]', ), ))
    # mdl.rootAssembly.Surface(name='Surf-11', side1Faces=
    #     mdl.rootAssembly.instances['Part-3-1'].faces.getSequenceFromMask(
    #     ('[#0 #1800 ]', ), ))
    # mdl.ContactProperty('IntProp-1')
    # mdl.interactionProperties['IntProp-1'].NormalBehavior(
    #     allowSeparation=ON, constraintEnforcementMethod=DEFAULT,
    #     pressureOverclosure=HARD)
    # mdl.ContactExp(createStepName='disp', name='Int-1')
    # mdl.interactions['Int-1'].includedPairs.setValuesInStep(
    #     addPairs=((mdl.rootAssembly.surfaces['Surf-10'],
    #     mdl.rootAssembly.surfaces['Surf-11']), ), stepName='disp'
    #     , useAllstar=OFF)
    # mdl.interactions['Int-1'].contactPropertyAssignments.appendInStep(
    #     assignments=((GLOBAL, SELF, 'IntProp-1'), ), stepName='disp')
    # del mdl.interactions['Int-1']
    # mdl.rootAssembly.Surface(name='m_Surf-12', side1Faces=
    #     mdl.rootAssembly.instances['Part-1-1'].faces.getSequenceFromMask(
    #     ('[#100 ]', ), ))
    # mdl.rootAssembly.Surface(name='s_Surf-12', side1Faces=
    #     mdl.rootAssembly.instances['Part-3-1'].faces.getSequenceFromMask(
    #     ('[#0 #800 ]', ), ))
    # mdl.Tie(adjust=ON, master=
    #     mdl.rootAssembly.surfaces['m_Surf-12'], name=
    #     'Constraint-7', positionToleranceMethod=COMPUTED, slave=
    #     mdl.rootAssembly.surfaces['s_Surf-12'], thickness=ON,
    #     tieRotations=ON)
    # mdl.steps['disp'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 10000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.fieldOutputRequests['F-Output-1'].setValues(timeInterval=
    #     1.0)
    # mdl.boundaryConditions['BC-4'].setValuesInStep(stepName=
    #     'disp', ur1=0.1)
    # mdl.steps['disp'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 100000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.fieldOutputRequests['F-Output-1'].setValues(timeInterval=
    #     2.0)
    # mdl.materials['Material-1'].Plastic(table=((426.0, 0.0), (
    #     429.0, 0.02), (450.0, 0.0234), (476.6, 0.0297), (500.0, 0.0365), (524.7,
    #     0.0447), (550.0, 0.056), (571.2, 0.067), (591.0, 0.08), (606.7, 0.093), (
    #     625.5, 0.112), (640.6, 0.1299), (654.9, 0.1495), (655.0, 0.2)))
    # mdl.steps['disp'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 1000000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.steps['disp'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 100000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.steps['disp'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 100000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ),
    #     timePeriod=50.0)
    # mdl.amplitudes['Amp-1'].setValues(data=((0.0, 0.0), (50.0,
    #     1.0)), timeSpan=STEP)
    # mdl.fieldOutputRequests['F-Output-1'].setValues(timeInterval=
    #     EVERY_TIME_INCREMENT)
    # mdl.rootAssembly.Set(name='rp_set', referencePoints=(
    #     mdl.rootAssembly.referencePoints[30], ))
    # mdl.FieldOutputRequest(createStepName='disp', name='ld',
    #     rebar=EXCLUDE, region=mdl.rootAssembly.sets['rp_set'],
    #     sectionPoints=DEFAULT, variables=('U', 'RF'))
    # mdl.fieldOutputRequests['F-Output-1'].setValues(numIntervals=
    #     20)
    # mdl.steps['disp'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 100000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ),
    #     timePeriod=20.0)
    # mdl.amplitudes['Amp-1'].setValues(data=((0.0, 0.0), (20.0,
    #     1.0)), timeSpan=STEP)
    # del mdl.fieldOutputRequests['ld']
    # mdl.HistoryOutputRequest(createStepName='disp', frequency=1,
    #     name='ld', rebar=EXCLUDE, region=
    #     mdl.rootAssembly.sets['rp_set'], sectionPoints=DEFAULT,
    #     variables=('U1', 'U2', 'U3', 'UR1', 'UR2', 'UR3', 'RF1', 'RF2', 'RF3',
    #     'RM1', 'RM2', 'RM3'))
    # mdl.steps['disp'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 500000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.parts['Part-3'].HoleThruAllFromEdges(diameter=56.0,
    #     distance1=28.0, distance2=0.0, edge1=
    #     mdl.parts['Part-3'].edges[40], edge2=
    #     mdl.parts['Part-3'].edges[51], plane=
    #     mdl.parts['Part-3'].faces[53], planeSide=SIDE1)
    # mdl.parts['Part-3'].features['Cut circular hole-1'].setValues(
    #     distFromE1=30.0)
    # mdl.parts['Part-3'].regenerate()
    # mdl.parts['Part-3'].regenerate()
    # mdl.parts['Part-3'].DatumPointByOffset(point=
    #     mdl.parts['Part-3'].vertices[4], vector=(0.0, -2.0, 0.0))
    # mdl.parts['Part-3'].DatumPlaneByPointNormal(normal=
    #     mdl.parts['Part-3'].edges[13], point=
    #     mdl.parts['Part-3'].datums[42])
    # mdl.parts['Part-3'].deleteMesh(regions=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#3dd ]',
    #     ), ))
    # mdl.parts['Part-3'].PartitionCellByDatumPlane(cells=
    #     mdl.parts['Part-3'].cells.getSequenceFromMask(('[#7a ]',
    #     ), ), datumPlane=mdl.parts['Part-3'].datums[43])
    # mdl.parts['Part-3'].generateMesh()
    # mdl.rootAssembly.regenerate()
    # mdb.jobs.changeKey(fromName='wrhs_job', toName='whrhs_job')
    # mdl.ExplicitDynamicsStep(massScaling=((SEMI_AUTOMATIC, MODEL,
    #     AT_BEGINNING, 100000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ), name=
    #     'axial', previous='Initial', timePeriod=10.0)
    # mdl.steps['axial'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 500000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.SmoothStepAmplitude(data=((0.0, 0.0), (10.0, 1.0)), name=
    #     'Amp-2', timeSpan=STEP)
    # del mdl.boundaryConditions['BC-4']
    # mdl.DisplacementBC(amplitude='Amp-1', createStepName='disp',
    #     distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=None, name=
    #     'BC-4', region=mdl.rootAssembly.sets['rp_set'], u1=0.0,
    #     u2=UNSET, u3=UNSET, ur1=0.1, ur2=0.0, ur3=0.0)
    # del mdl.boundaryConditions['BC-3']
    # mdl.rootAssembly.Set(name='Set-10', referencePoints=(
    #     mdl.rootAssembly.referencePoints[32], ))
    # mdl.DisplacementBC(amplitude=UNSET, createStepName='Initial',
    #     distributionType=UNIFORM, fieldName='', localCsys=None, name='BC-5',
    #     region=mdl.rootAssembly.sets['Set-10'], u1=SET, u2=SET,
    #     u3=SET, ur1=UNSET, ur2=UNSET, ur3=UNSET)
    # mdl.rootAssembly.Set(name='Set-11', referencePoints=(
    #     mdl.rootAssembly.referencePoints[34], ))
    # mdl.DisplacementBC(amplitude=UNSET, createStepName='Initial',
    #     distributionType=UNIFORM, fieldName='', localCsys=None, name='BC-6',
    #     region=mdl.rootAssembly.sets['Set-11'], u1=SET, u2=SET,
    #     u3=SET, ur1=UNSET, ur2=UNSET, ur3=UNSET)
    # mdl.boundaryConditions['BC-5'].setValuesInStep(stepName=
    #     'axial', u3=FREED)
    # del mdl.boundaryConditions['BC-5']
    # mdl.DisplacementBC(amplitude=UNSET, createStepName='axial',
    #     distributionType=UNIFORM, fieldName='', fixed=OFF, localCsys=None, name=
    #     'BC-5', region=mdl.rootAssembly.sets['Set-10'], u1=0.0,
    #     u2=0.0, u3=UNSET, ur1=UNSET, ur2=UNSET, ur3=UNSET)
    # mdl.ConcentratedForce(amplitude='Amp-2', cf3=-350000.0,
    #     createStepName='axial', distributionType=UNIFORM, field='', localCsys=None,
    #     name='Load-1', region=mdl.rootAssembly.sets['Set-10'])
    # mdl.loads['Load-1'].setValues(cf3=350000.0, distributionType=
    #     UNIFORM, field='')
    # mdl.FieldOutputRequest(createStepName='axial', name=
    #     'F-Output-2', variables=('S', 'MISES', 'E', 'PE', 'PEEQ', 'U'))
    # mdl.HistoryOutputRequest(createStepName='axial', frequency=1,
    #     name='axial', rebar=EXCLUDE, region=
    #     mdl.rootAssembly.sets['Set-10'], sectionPoints=DEFAULT,
    #     variables=('U1', 'U2', 'U3', 'UR1', 'UR2', 'UR3', 'RF1', 'RF2', 'RF3',
    #     'RM1', 'RM2', 'RM3'))
    # mdl.loads['Load-1'].setValuesInStep(cf3=80.0, stepName=
    #     'disp')
    # mdl.steps['axial'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 100000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.steps['axial'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 10000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.amplitudes['Amp-2'].setValues(data=((0.0, 0.0), (8.0,
    #     1.0)), timeSpan=STEP)
    # mdl.amplitudes['Amp-1'].setValues(data=((0.0, 0.0), (18.0,
    #     1.0)), timeSpan=STEP)
    # mdl.steps['axial'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 1000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # del mdl.loads['Load-1']
    # mdl.rootAssembly.Surface(name='Surf-14', side1Faces=
    #     mdl.rootAssembly.instances['Part-1-1'].faces.getSequenceFromMask(
    #     ('[#8000000 ]', ), ))
    # mdl.Pressure(amplitude='Amp-2', createStepName='axial',
    #     distributionType=UNIFORM, field='', magnitude=50.0, name='Load-1', region=
    #     mdl.rootAssembly.surfaces['Surf-14'])
    # mdl.steps['axial'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 1000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ),
    #     timePeriod=20.0)
    # mdl.amplitudes['Amp-2'].setValues(data=((0.0, 0.0), (18.0,
    #     1.0)), timeSpan=STEP)
    # del mdl.loads['Load-1']
    # mdl.boundaryConditions['BC-5'].setValues(amplitude='Amp-2',
    #     u3=0.1)
    # mdl.steps['axial'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 5000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.amplitudes['Amp-2'].setValues(data=((0.0, 0.0), (18.0,
    #     1.0), (20.0, 1.0)), timeSpan=STEP)
    # mdl.amplitudes['Amp-1'].setValues(data=((0.0, 0.0), (18.0,
    #     1.0), (20.0, 1.0)), timeSpan=STEP)
    # mdl.steps['axial'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 1000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # del mdl.boundaryConditions['BC-5']
    # mdl.DisplacementBC(amplitude=UNSET, createStepName='Initial',
    #     distributionType=UNIFORM, fieldName='', localCsys=None, name='BC-5',
    #     region=mdl.rootAssembly.sets['Set-6'], u1=SET, u2=SET,
    #     u3=SET, ur1=UNSET, ur2=UNSET, ur3=UNSET)
    # mdl.boundaryConditions['BC-5'].setValues(region=
    #     mdl.rootAssembly.sets['Set-10'])
    # mdl.boundaryConditions['BC-6'].setValuesInStep(amplitude=
    #     'Amp-2', stepName='axial', u3=-0.1)
    # mdl.steps['axial'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 10000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.rootAssembly.Set(faces=
    #     mdl.rootAssembly.instances['Part-1-1'].faces.getSequenceFromMask(
    #     mask=('[#201040 ]', ), )+\
    #     mdl.rootAssembly.instances['Part-3-1'].faces.getSequenceFromMask(
    #     mask=('[#5000440 #4400420 #14000 ]', ), ), name='Set-5')
    # mdl.steps['axial'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 10000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ),
    #     timePeriod=10.0)
    # mdl.amplitudes['Amp-2'].setValues(data=((0.0, 0.0), (5.0,
    #     1.0), (10.0, 1.0)), timeSpan=STEP)
    # mdl.boundaryConditions['BC-6'].setValuesInStep(stepName=
    #     'axial', u3=-0.2)
    # mdl.steps['axial'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 10000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ),
    #     timePeriod=5.0)
    # mdl.amplitudes['Amp-2'].setValues(data=((0.0, 0.0), (5.0,
    #     1.0)), timeSpan=STEP)
    # mdl.steps['disp'].resume()
    # mdl.fieldOutputRequests['F-Output-1'].resume()
    # del mdl.fieldOutputRequests['F-Output-1']
    # del mdl.historyOutputRequests['axial']
    # mdl.HistoryOutputRequest(createStepName='axial', name='axial'
    #     , rebar=EXCLUDE, region=mdl.rootAssembly.sets['Set-11'],
    #     sectionPoints=DEFAULT, variables=('U1', 'U2', 'U3', 'UR1', 'UR2', 'UR3',
    #     'RF1', 'RF2', 'RF3', 'RM1', 'RM2', 'RM3'))
    # mdl.boundaryConditions['BC-6'].setValuesInStep(stepName=
    #     'axial', u3=-0.4)
    # mdl.rootAssembly.sets.changeKey(fromName='Set-4', toName=
    #     'y_symm_set')
    # mdl.rootAssembly.sets.changeKey(fromName='Set-5', toName=
    #     'x_symm_set')
    # mdl.rootAssembly.sets.changeKey(fromName='Set-7', toName=
    #     'beam_end_set')
    # mdl.rootAssembly.sets.changeKey(fromName='Set-10', toName=
    #     'fix_end_set')
    # mdl.rootAssembly.sets.changeKey(fromName='Set-11', toName=
    #     'disp_end_set')
    # del mdl.rootAssembly.sets['rp_set']
    # del mdl.rootAssembly.sets['Set-6']
    # mdl.rootAssembly.Set(name='hist_set', referencePoints=(
    #     mdl.rootAssembly.referencePoints[30],
    #     mdl.rootAssembly.referencePoints[34]))
    # mdl.steps.changeKey(fromName='axial', toName='step_1')
    # mdl.steps.changeKey(fromName='disp', toName='step_2')
    # mdl.steps['step_1'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 1000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ),
    #     timePeriod=1.0)
    # mdl.steps['step_2'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 1000000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ),
    #     timePeriod=1.0)
    # del mdl.historyOutputRequests['H-Output-1']
    # del mdl.historyOutputRequests['axial']
    # del mdl.historyOutputRequests['ld']
    # mdl.HistoryOutputRequest(createStepName='step_1', name='hist'
    #     , rebar=EXCLUDE, region=mdl.rootAssembly.sets['hist_set']
    #     , sectionPoints=DEFAULT, variables=('U1', 'U2', 'U3', 'UR1', 'UR2', 'UR3',
    #     'RF1', 'RF2', 'RF3', 'RM1', 'RM2', 'RM3', 'CF1', 'CF2', 'CF3', 'CM1',
    #     'CM2', 'CM3'))
    # mdl.amplitudes['Amp-1'].setValues(data=((0.0, 0.0), (1.0,
    #     1.0)), timeSpan=STEP)
    # mdl.amplitudes['Amp-2'].setValues(data=((0.0, 0.0), (1.0,
    #     1.0)), timeSpan=STEP)
    # mdl.boundaryConditions['BC-1'].setValues(region=
    #     mdl.rootAssembly.sets['y_symm_set'])
    # mdl.boundaryConditions.changeKey(fromName='BC-1', toName=
    #     'y_symm')
    # mdl.boundaryConditions['BC-2'].setValues(region=
    #     mdl.rootAssembly.sets['x_symm_set'])
    # mdl.boundaryConditions.changeKey(fromName='BC-2', toName=
    #     'x_symm')
    # del mdl.boundaryConditions['BC-4']
    # del mdl.boundaryConditions['BC-5']
    # del mdl.boundaryConditions['BC-6']
    # mdl.DisplacementBC(amplitude=UNSET, createStepName='Initial',
    #     distributionType=UNIFORM, fieldName='', localCsys=None, name='fix_end',
    #     region=mdl.rootAssembly.sets['fix_end_set'], u1=SET, u2=
    #     SET, u3=SET, ur1=SET, ur2=SET, ur3=SET)
    # mdl.DisplacementBC(amplitude=UNSET, createStepName='Initial',
    #     distributionType=UNIFORM, fieldName='', localCsys=None, name='disp_end',
    #     region=mdl.rootAssembly.sets['disp_end_set'], u1=SET, u2=
    #     SET, u3=SET, ur1=SET, ur2=SET, ur3=SET)
    # mdl.boundaryConditions['disp_end'].setValuesInStep(amplitude=
    #     'Amp-2', stepName='step_2', u3=-2.0)
    # mdl.Moment(cm1=6500000.0, createStepName='step_1',
    #     distributionType=UNIFORM, field='', localCsys=None, name='Load-1', region=
    #     mdl.rootAssembly.sets['beam_end_set'])
    # mdl.steps['step_1'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 100000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.steps['step_2'].setValues(massScaling=((SEMI_AUTOMATIC,
    #     MODEL, AT_BEGINNING, 100000.0, 0.0, None, 0, 0, 0.0, 0.0, 0, None), ))
    # mdl.loads['Load-1'].setValues(amplitude='Amp-1',
    #     distributionType=UNIFORM, field='')
    # del mdl.materials['Material-1'].plastic
    # mdl.materials['Material-1'].Plastic(table=((300.0, 0.0), (
    #     400.0, 4.53e-05), (450.0, 8.24e-05), (500.0, 0.000141), (550.0, 0.000247),
    #     (600.0, 0.000445), (630.0, 0.000652), (660.0, 0.001), (700.0, 0.00207), (
    #     720.0, 0.00337), (750.0, 0.00918), (770.0, 0.0141), (790.0, 0.0201), (
    #     800.0, 0.0238), (820.0, 0.0326), (840.0, 0.0456), (850.0, 0.0552), (860.0,
    #     0.0711), (865.0, 0.0934)))
