      SUBROUTINE URDFIL(LSTOP,LOVRWRT,KSTEP,KINC,DTIME,TIME)
C     Subroutine to terminate an abaqus Riks analysis.
C     Header taken from Abaqus user's manual examples.
      INCLUDE 'ABA_PARAM.INC'
      DIMENSION ARRAY(513),JRRAY(NPRECD,513),TIME(2)
      EQUIVALENCE (ARRAY(1),JRRAY(1,1))
C     My vars
      DOUBLE PRECISION, DIMENSION(100) :: THE_LPF
      DATA THE_LPF/100*0.D0/
      CHARACTER(LEN=8) H1, H2, H3, H4
      CHARACTER(LEN=36) HEADING      
C     Loop over a large number and call the .odb file
      PRINT *, "PM-> Message from outside the loop"
      DO K = 1, 999999
          CALL DBFILE(0,ARRAY,JRCD)
          IF (JRCD.NE.0) GO TO 110
          KEY = JRRAY(1,2)
          PRINT *, KEY
C     LPF is stored in Record Key 2000 attribute 9 + offset 2 = array position 11
          IF (KEY.EQ.2000) THEN
              THE_LPF(KINC) = ARRAY(11)
              PRINT *, "PM-> From this point until STOP, debug printing"
              PRINT *, "Position 00: ", ARRAY(0)
              PRINT *, "Position 01: ", ARRAY(1)
              PRINT *, "Position 02: ", ARRAY(2)
              PRINT *, "Position 03: ", ARRAY(3)
              PRINT *, "Position 04: ", ARRAY(4)
              PRINT *, "Position 05: ", ARRAY(5)
              PRINT *, "Position 06: ", ARRAY(6)
              PRINT *, "Position 07: ", ARRAY(7)
              PRINT *, "Position 08: ", ARRAY(8)
              PRINT *, "Position 09: ", ARRAY(9)
              PRINT *, "Position 10: ", ARRAY(10)
              PRINT *, "Position 11: ", ARRAY(11)
              PRINT *, "Position 12: ", ARRAY(12)
              PRINT *, "Position 13: ", ARRAY(13)
              PRINT *, "PM-> Increment: ", KINC
              PRINT *, "Current LPF: ", THE_LPF(KINC)
              PRINT *, "STOP"
C     The contents of the *Heading keyword are stored in Record Key 1922 at array positions 3 onwards
          ELSE IF (KEY.EQ.1922) THEN
              WRITE(H1,'(A8)') ARRAY(3)
              WRITE(H2,'(A8)') ARRAY(4)
              WRITE(H3,'(A8)') ARRAY(5)  
              WRITE(H4,'(A8)') ARRAY(6)              
              HEADING = H1 // H2 // H3 // H4
              PRINT *, "PM-> The heading is: ", HEADING
C     Note that as many H's should be used to copy the full .inp path and name root, since each ARRAY(X) holds only 8 characters    
C     e.g. if path is short and fits into H1 only, HEADING need only be of length 8 etc.         
          END IF          
      END DO
110   CONTINUE
C     Condition for solver termination
      IF (KINC.GE.2) THEN
          IF (THE_LPF(KINC).GE.THE_LPF(KINC-1)) THEN
              PRINT *, "PM-> Step, current and previous LPF: "
              PRINT *,KINC, THE_LPF(KINC), THE_LPF(KINC-1)
              CONTINUE
          ELSE
              LSTOP = 1; PRINT *, "Riks solver terminated by subroutine"
          END IF
      END IF
      LOVRWRT = 1
      RETURN
      END
