import os
import Pabq.common_tools.factorial_exec as fe

os.chdir("../fruits")

#path = at.find(prj_name + "_batch_status.csv", "../../")
#print(path)
#os.chdir(path)

def wrapper(*args, **kargs):
    print("Im in")
    for root, dirs, files in os.walk("."):
        for file in files:
            if file[-3:]=="inp":
                cmd = "abaqus job=" + file[:-4] + " interactive"
                print(cmd)
                os.system(cmd)

    return "done!"


# Execute the full factorial for the given value ranges.
# The given input must be identical to those given in the initial execution
# that created the existing jobs.
fe.run_factorial(
   "ff",
    wrapper,
    func_args=[
        [20, 30, 40],
        [100, 90],
        [10]
    ],
    func_kargs={
        "ncpus": 4,
    },
    #func_kargs={"ncpus": 1},
    mk_subdirs=True,
    p_args=[0, 1, 2]
)
