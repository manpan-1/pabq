## -*- coding: utf-8 -*-
#
#"""Sub-package of `Pabq` containing tools for Abaqus that are used across
#multiple modellers."""
#
#__author__ = """Panagiotis Manoleas"""
#__email__ = 'panagiotis.manoleas@paramatrixab.com'
#__version__ = '0.1'
#__release__ = '0.1.0'
#__all__ = [
#    'abq_tools',
#    'design_of_joints',
#    'factorial_exec',
#    'steel_design',
#    'submit_existing'
#]
#
#import Pabq.common_tools.abq_tools
#
